# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import bpy
import importlib

from . import operators

from bpy.app.handlers import persistent

importlib.reload(operators)

bl_info = {
    "name": "CacheTool",
    "author": "Andy Goralczyk",
    "description": "",
    "blender": (2, 80, 0),
    "version": (0, 0, 1),
    "location": "",
    "warning": "",
    "category": "Workflow"
}


@persistent
def update_cache_list(self, context):
    """Makes sure the list of cachable collections is only updated on depsgraph update"""
    cache_collections = []
    for collection in bpy.data.collections:
        if ".cache" in collection.name and collection.override_library:
            cache_collections.append(collection)
    bpy.context.scene['cache_collections'] = cache_collections


class CT_CacheCollectionSettings(bpy.types.PropertyGroup):
    is_cached: bpy.props.BoolProperty(
        name='is cached',
        description="Cache status of collection",
        default=False
    )
    is_outdated: bpy.props.BoolProperty(
        name='outdated',
        description="Matches latest cache",
        default=True
    )


class CT_PT_CacheList(bpy.types.Panel):
    bl_label = "Cache Tool"
    bl_idname = "OBJECT_PT_CachePanel"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = "Cache"
    bl_context = 'objectmode'

    def draw(self, context):
        layout = self.layout

        row = layout.row()
        row.operator("cachetool.cache_import")
        if context.scene['cache_collections']:
            row = layout.row()
            row.label(text="Cache collections:")
        for collection in context.scene['cache_collections']:
            box = layout.box()
            row = box.row()
            if collection.cache_properties.is_cached:
                row.label(text="", icon='FILE_CACHE')
            else:
                row.label(text="", icon='ARMATURE_DATA')

            # Make the label text more pretty
            label_name = collection.name.replace("_", " ")
            label_name = label_name.replace(".cache", "")
            label_name = label_name.capitalize()
            row.label(text=label_name)

            if collection.cache_properties.is_cached:
                free_single = row.operator('cachetool.cache_free_single',
                                           text="", icon='TRASH', emboss=False)
                free_single.collection = collection.name
            else:
                cache_single = row.operator(
                    "cachetool.cache_single_collection", text="", icon='EXPORT')
                cache_single.collection = collection.name
        if context.scene['cache_collections']:
            row = layout.row()
            row.operator("cachetool.cache_all_collections")


class CT_Preferences(bpy.types.AddonPreferences):
    bl_idname = __name__

    export_root: bpy.props.StringProperty(
        name="Cache export path",
        description="Default path to store cache files in",
        default="//",
        subtype='DIR_PATH'
    )
    work_cache_root: bpy.props.StringProperty(
        name="Work cache path",
        description="Path to store (local) work caches in",
        default="//",
        subtype='DIR_PATH'
    )

    def draw(self, context):
        layout = self.layout
        row = layout.row()
        row.prop(self, 'export_root')
        row = layout.row()
        row.prop(self, 'work_cache_root')
        row = layout.row()


classes = (
    CT_PT_CacheList,
    CT_CacheCollectionSettings,
    CT_Preferences,
)


def register():
    operators.register()
    for cls in classes:
        bpy.utils.register_class(cls)

    bpy.types.Collection.cache_properties = bpy.props.PointerProperty(
        type=CT_CacheCollectionSettings)
    bpy.app.handlers.depsgraph_update_post.append(update_cache_list)


def unregister():
    operators.unregister()
    for cls in classes:
        bpy.utils.unregister_class(cls)
    del bpy.types.Collection.cache_properties


if __name__ == "__main__":
    register()
