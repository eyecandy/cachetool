import bpy
import bpy_extras
import os
import importlib

from . import cache

importlib.reload(cache)


class CACHE_OT_CacheSingle(bpy.types.Operator):
    """Write alembic cache of specific collection to disk"""
    bl_idname = "cachetool.cache_single_collection"
    bl_label = "Cache Single"

    collection: bpy.props.StringProperty()

    @classmethod
    def poll(cls, context):
        return context.selected_objects is not None and context.mode == 'OBJECT'

    def execute(self, context):
        bpy.ops.object.select_all(action='DESELECT')
        current_collection = bpy.data.collections[self.collection]
        cache.select_objects_in(current_collection)
        # Disable all modifiers we don't want to export
        cache.set_state_export(context.selected_objects)

        # Give the cache the name of the current collection minus .cache
        # should be the asset name, e.g. 'CH-spring' contains 'spring.cache'
        asset_name = self.collection.replace(".cache", "")
        cache.generate_cache(cache_name=asset_name, cache_type='WORK_CACHE')

        cache.set_state_import(context.selected_objects)
        cache_file_name = cache.load_work_cache(cache_name=asset_name)
        cache.apply_cachefile(context.selected_objects, cache_file_name)

        # Set the asset cache collection to status 'cached' so it can be reflected in the UI
        current_collection.cache_properties.is_cached = True
        return {'FINISHED'}


class CACHE_OT_FreeSingle(bpy.types.Operator):
    """Remove cache from specified collection. This does not delete the cache files for safety reasons."""
    bl_idname = "cachetool.cache_free_single"
    bl_label = "Free Cache"

    collection: bpy.props.StringProperty()

    @classmethod
    def poll(cls, context):
        return context.selected_objects is not None and context.mode == 'OBJECT'

    def execute(self, context):
        bpy.ops.object.select_all(action='DESELECT')
        cache.select_objects_in(bpy.data.collections[self.collection])

        cache.set_state_default(bpy.context.selected_objects)
        cache.remove_cachefile(context.selected_objects)
        # Set the asset cache collection to status 'cached' so it can be reflected in the UI
        bpy.data.collections[self.collection].cache_properties.is_cached = False

        return {'FINISHED'}


class CACHE_OT_CacheAllCollections(bpy.types.Operator):
    """Write alembic cache of selected objects to disk"""
    bl_idname = "cachetool.cache_all_collections"
    bl_label = "Cache All"

    @classmethod
    def poll(cls, context):
        return context.selected_objects is not None and context.mode == 'OBJECT'

    def execute(self, context):
        bpy.ops.object.select_all(action='DESELECT')
        for coll in context.scene['cache_collections']:
            cache.select_objects_in(coll)

        # Disable all modifiers we don't want to export
        cache.set_state_export(context.selected_objects)

        # Give the cache the name of the current collection minus .cache
        # should be the asset name, e.g. 'CH-spring' contains 'spring.cache'

        cache_dir = cache.generate_cache(cache_name="ANIM",
                                         cache_type='SHOT_CACHE')

        cache.set_state_default(context.selected_objects)
        self.report({'WARNING', 'INFO'}, f"Cache exported to {cache_dir}")
        return {'FINISHED'}


class CACHE_OT_CacheImport(bpy.types.Operator, bpy_extras.io_utils.ImportHelper):
    bl_idname = "cachetool.cache_import"
    bl_label = "Import Cache"

    def execute(self, context):

        # If a file was selected, remove it from the path. Also add trailing slash
        cache_dir, _ = os.path.split(self.filepath)
        cache_dir += "/"

        meta_file_path = cache_dir + "meta.json"
        metadata = cache.read_meta_from_json(meta_file_path)

        alembic_file_path = metadata['alembic_file']
        bpy.ops.cachefile.open(filepath=alembic_file_path)

        alembic_file_name = alembic_file_path.replace(cache_dir, "")

        libraries = metadata['library_files']
        for coll_name, lib in libraries.items():
            with bpy.data.libraries.load(lib, link=True) as (data_from, data_to):
                data_to.collections = [
                    name for name in data_from.collections if name.startswith(coll_name)]

            coll_override = bpy.data.collections[coll_name].override_create(
                remap_local_usages=True)
            for ob in coll_override.objects:
                try:
                    for mat in ob.data.materials:
                        mat.name = cache.format_alembic_name(mat.name)
                except AttributeError:
                    continue
            coll_override.name = coll_name
            coll_override.hide_render = False
            coll_override.hide_viewport = False
            context.scene.collection.children.link(coll_override)

            for ob in coll_override.objects:
                ob.override_create(remap_local_usages=True)

                # Cameras can be made local since they are pretty simple to maintain
                if ob.type == 'CAMERA':
                    ob.make_local()
                    ob.data.make_local()

            cache.set_state_import(coll_override.objects)

            cache.apply_cachefile(coll_override.objects, alembic_file_name)

        # bpy.ops.wm.alembic_import(filepath=alembic_file_path, check_existing=True, filter_blender=False, filter_backup=False, filter_image=False, filter_movie=False,
        #                           filter_python=False, filter_font=False, filter_sound=False, filter_text=False, filter_archive=False, filter_btx=False, filter_collada=False,
        #                           filter_alembic=True, filter_usd=False, filter_volume=False, filter_folder=True, filter_blenlib=False, filemode=8,
        #                           relative_path=False, display_type='DEFAULT', sort_method='FILE_SORT_ALPHA', scale=1, set_frame_range=True, validate_meshes=False,
        #                           is_sequence=False, as_background_job=False)

        driver_file_path = cache_dir + "drivers.json"
        if os.path.exists(driver_file_path):
            cache.read_drivers_from_json(context, driver_file_path)
        return {'FINISHED'}


classes = (
    CACHE_OT_CacheSingle,
    CACHE_OT_FreeSingle,
    CACHE_OT_CacheAllCollections,
    CACHE_OT_CacheImport,
)


def register():
    for cls in classes:
        bpy.utils.register_class(cls)


def unregister():
    for cls in classes:
        bpy.utils.unregister_class(cls)


if __name__ == "__main__":
    register()
