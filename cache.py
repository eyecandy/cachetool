import bpy
import os
import json

from datetime import datetime

# Create a dict that will contain all the driven values
# TODO! Make sure the variables get properly cleared when file is reloaded

output = {}
driven_properties = []

material_cache_objects = {'MESH', 'CURVE', 'META'}
modifier_objects = {'MESH', 'CURVE'}

modifiers_not_exported = {
    'MESH_SEQUENCE_CACHE', 'SUBSURF', 'PARTICLE_SYSTEM', 'MASK'}
constraints_not_exported = {'TRANSFORM_CACHE'}

modifiers_default_disabled = {'MESH_SEQUENCE_CACHE'}
constraints_default_disabled = {'TRANSFORM_CACHE'}


def select_objects_in(collection):
    """Selects objects located in one or more collections"""
    for ob in collection.objects:
        ob.select_set(True)


def set_state_default(objects):
    for ob in objects:
        if ob.type not in modifier_objects:
            continue
        for mod in ob.modifiers:
            if mod.type in modifiers_default_disabled:
                mod.show_render = False
                mod.show_viewport = False
            else:
                mod.show_render = True
                mod.show_viewport = True
    for const in ob.constraints:
        if const.type in constraints_default_disabled:
            const.mute = True
        else:
            const.mute = False


def set_state_export(objects):
    """Disable the modifiers not needed for export"""
    # TODO: check if modifiers are disabled for view/render
    #       and only toggle them on when they are not off
    for ob in objects:
        if ob.type not in modifier_objects:
            continue
        for mod in ob.modifiers:
            if mod.type in modifiers_not_exported:
                mod.show_render = False
                mod.show_viewport = False
            else:
                mod.show_render = True
                mod.show_viewport = True
    for const in ob.constraints:
        if const.type in constraints_not_exported:
            const.mute = True
        else:
            const.mute = False


def set_state_import(objects):
    """Enable the modifiers not needed for export"""
    for ob in objects:
        if ob.type not in modifier_objects:
            continue
        for mod in ob.modifiers:
            if mod.type in modifiers_not_exported:
                mod.show_render = True
                mod.show_viewport = True
            else:
                mod.show_render = False
                mod.show_viewport = False
    for const in ob.constraints:
        if const.type in constraints_not_exported:
            const.mute = False
        else:
            const.mute = True


def apply_cachefile(objects, cache_file_name):
    """Simple loading of mesh and camera objects"""
    cache_file = bpy.data.cache_files[cache_file_name]
    for ob in objects:
        if ob.type in modifier_objects:
            for mod in ob.modifiers:
                if mod.type == 'MESH_SEQUENCE_CACHE':
                    mod.cache_file = cache_file
                    mod.object_path = f"/{ob.name}/{ob.data.name}"
                    if ob.parent:
                        mod.object_path = f"/{ob.parent.name}{mod.object_path}"
                    mod.object_path = format_alembic_name(mod.object_path)
        elif ob.type == 'CAMERA':
            ob.data.name = f"{ob.data.name}"
            for const in ob.constraints:
                if const.type in constraints_not_exported:
                    const.cache_file = cache_file
                    const.object_path = f"/{ob.name}/{ob.data.name}"
                    if ob.parent:
                        const.object_path = f"/{ob.parent.name}{const.object_path}"
                    const.object_path = format_alembic_name(const.object_path)
        else:
            continue


def remove_cachefile(objects):
    """Remove cache file reference from modifiers"""
    for ob in objects:
        if ob.type in modifier_objects:
            for mod in ob.modifiers:
                if mod.type == 'MESH_SEQUENCE_CACHE':
                    mod.cache_file = None
                    mod.object_path = ""
        elif ob.type == 'CAMERA':
            # removing of 'Shape' is not necessary anymore since 2.90
            # ob.data.name = ob.data.name.replace("Shape", "")
            for const in ob.constraints:
                if const.type in constraints_not_exported:
                    const.cache_file = None
                    const.object_path = ""
        else:
            continue


def load_work_cache(cache_name: str):
    context = bpy.context
    current_file_name = bpy.path.basename(
        context.blend_data.filepath).replace(".blend", "")

    preferences = context.preferences.addons['cachetool'].preferences
    cache_dir = bpy.path.abspath(preferences.work_cache_root)
    cache_dir += f"cache_{current_file_name}_{cache_name}/"

    alembic_file_path = f"{cache_dir}{current_file_name}_{cache_name}.abc"
    bpy.ops.cachefile.open(filepath=alembic_file_path)
    cache_file_name = f"{current_file_name}_{cache_name}.abc"

    return cache_file_name


def generate_cache(cache_name: str, cache_type='WORK_CACHE'):
    context = bpy.context
    current_file_name = bpy.path.basename(
        context.blend_data.filepath).replace(".blend", "")

    preferences = context.preferences.addons['cachetool'].preferences

    if cache_type == 'SHOT_CACHE':
        cache_dir = bpy.path.abspath(preferences.export_root)
    elif cache_type == 'WORK_CACHE':
        cache_dir = bpy.path.abspath(preferences.work_cache_root)
    else:
        return

    cache_dir += f"cache_{current_file_name}_{cache_name}/"

    if not os.path.exists(cache_dir):
        os.mkdir(cache_dir)

    meta_file_path = f"{cache_dir}meta.json"
    driver_file_path = f"{cache_dir}drivers.json"
    alembic_file_path = f"{cache_dir}{current_file_name}_{cache_name}.abc"

    library_files = get_libraries(context.selected_objects)

    metadata = {
        'name': cache_name,
        'creation_date': datetime.today().strftime('%Y-%m-%d-%H:%M:%S'),
        'alembic_file': alembic_file_path,
        'driver_file': driver_file_path,
        'library_files': library_files,
        'type': cache_type
    }

    write_meta_to_json(metadata, meta_file_path)

    if cache_type == 'SHOT_CACHE':
        output = get_driver_keyframes(
            context, context.selected_objects)
        if output:
            write_drivers_to_json(
                output, driver_file_path)

    bpy.ops.wm.alembic_export(filepath=alembic_file_path,
                              selected=True, renderable_only=True, visible_objects_only=False, flatten=False, uvs=True, packuv=True, normals=True,
                              vcolors=True, face_sets=True, subdiv_schema=False, apply_subdiv=False, start=context.scene.frame_start,
                              end=context.scene.frame_end, curves_as_mesh=False, global_scale=1, triangulate=False,
                              quad_method='SHORTEST_DIAGONAL', ngon_method='BEAUTY', export_hair=False,
                              export_particles=True, as_background_job=False, init_scene_frame_range=False)

    return cache_dir


def _initialize_dict(
    scene,
    object,
    data,
    driver
):
    key = _dictionary_key(data, driver)
    output[key] = {}
    output[key]['type'] = object.type
    output[key]['datablock_name'] = data.name
    output[key]['driver_data_path'] = driver.data_path
    output[key]['frame_start'] = scene.frame_start
    output[key]['frame_end'] = scene.frame_end
    output[key]['driven_value'] = []


def _dictionary_key(data, driver):
    """Returns a key so each entry in the output dict is unique"""
    return f'{data.name}______{driver.data_path}'


def get_driver_keyframes(context, objects):
    """Stores all driven properties of <objects> and returns a dict with the values"""
    for ob in objects:
        if ob.type in material_cache_objects:
            for material in ob.data.materials:
                if material.use_nodes and material.node_tree.animation_data:
                    for driver in material.node_tree.animation_data.drivers:
                        driven_properties.append([material, driver])
                        _initialize_dict(context.scene, ob,
                                         material, driver)
        else:
            if ob.data.animation_data.drivers:
                for driver in ob.data.animation_data.drivers:
                    driven_properties.append([ob.data, driver])
                    _initialize_dict(context.scene, ob,
                                     ob.data, driver)
    # Abort if there are no drivers at all, not necessary to iterate through all frames
    if not driven_properties:
        return None

    scene = context.scene
    # remember frame before executing
    frame_before_running = scene.frame_current

    # Now iterate through frames and store driver values
    for frame in range(scene.frame_start, scene.frame_end):
        scene.frame_set(frame)

        for data, driver in driven_properties:
            try:
                driven_value = data.node_tree.path_resolve(driver.data_path)
            except AttributeError:
                driven_value = data.path_resolve(driver.data_path)
            key = _dictionary_key(data, driver)
            output[key]['driven_value'].append(driven_value)

    scene.frame_set(frame_before_running)
    driven_properties.clear()

    return output


def get_libraries(objects):
    """Returns a dict of collections and their libraries used in this file"""

    libraries = {}
    for ob in objects:
        print(ob)
        if ob.override_library.reference.library:
            for coll in ob.users_collection:
                if coll.name.endswith('.cache'):
                    libraries[coll.name] = bpy.path.abspath(
                        ob.override_library.reference.library.filepath)

    return libraries


def write_drivers_to_json(output_dict, driver_file_path):

    print(f"Writing baked drivers to file: {driver_file_path}")

    with open(driver_file_path, 'w') as file:
        try:
            json.dump(output_dict, file, indent=4)
        except TypeError:
            print("Color and vector drivers are currently not supported!")
            os.remove(driver_file_path)


def write_meta_to_json(metadata, meta_file_path):
    with open(meta_file_path, 'w') as file:
        json.dump(metadata, file, indent=4)


def read_meta_from_json(meta_file_path):
    with open(meta_file_path) as file:
        metadata = json.load(file)
    return metadata


def format_alembic_name(format_string: str) -> str:
    replace = [' ', '.']
    for char in replace:
        format_string = format_string.replace(char, '_')
    return format_string


def read_drivers_from_json(context, driver_file_path):
    with open(driver_file_path) as file:
        input = json.load(file)
        for key in input:
            data_name = input[key]['datablock_name']
            data_type = input[key]['type']
            path = input[key]['driver_data_path']

            frame_start = input[key]['frame_start']
            frame_end = input[key]['frame_end']

            if data_type == 'CAMERA':
                data_name = format_alembic_name(data_name) + "Shape"
            if data_type == 'MESH':
                data_name = format_alembic_name(data_name)

            for frame in range(frame_start, frame_end):
                value = input[key]['driven_value'][frame - frame_start]

                if data_type == 'CAMERA':
                    camera = bpy.data.cameras[data_name]
                    exec(f"camera.{path} = {value}")
                    camera.keyframe_insert(
                        data_path=path, frame=frame, index=-1)
                if data_type == 'MESH':
                    material = bpy.data.materials[data_name]
                    material.use_nodes = True

                    exec(f"material.node_tree.{path} = {value}")
                    material.node_tree.keyframe_insert(
                        data_path=path, frame=frame, index=-1)
